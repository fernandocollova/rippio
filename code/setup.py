from setuptools import setup

setup(
    name='ripio',
    version="1.0.0",
    scripts=[
        "manage.py",
        "create_user.py",
    ],
    packages=[
        'ripio',
        'controllers',
        'db_model',
        'db_model.migrations',
        'db_model.models',
        'celery_tasks'
    ],
    install_requires=[
        "django<1.12",
        "djangorestframework<3.7",
        "celery<4.2",
        "django-celery-results<1.1",
    ]
)
