from django.test import TestCase
from db_model.models import (
    MintRequest,
    TransactionRequest,
    CoinTransaction,
    Coin,
    User,
)


class TestMintRequest(TestCase):

    def setUp(self):

        self.password = 'user1'
        self.user1 = User.objects.create_user(
            username='user1',
            password=self.password,
            email="user1@ripio.com",
            is_superuser=True,
            is_staff=True,
            balance=0
        )
        self.user1.save()

    def tearDown(self):
        self.user1.delete()
        for coin in Coin.objects.all():
            coin.delete()

    def test_mint_request_works(self):

        user1_previous_balance = self.user1.balance
        mint_request = MintRequest(amount=1, user=self.user1)

        mint_request.execute()
        mint_request.result.ready()
        self.user1.refresh_from_db()

        self.assertEqual(self.user1.balance, user1_previous_balance + 1)


class TestTransactionRequest(TestCase):

    def setUp(self):

        self.password = 'user1'
        self.user1 = User.objects.create_user(
            username='user1',
            password=self.password,
            email="user1@ripio.com",
            is_superuser=True,
            is_staff=True,
            balance=0
        )
        self.user1.save()

        Coin.objects.bulk_create([Coin(owner=self.user1) for _ in range(10)])

        self.password = 'user2'
        self.user2 = User.objects.create_user(
            username='user2',
            password=self.password,
            email="user2@ripio.com",
            is_superuser=True,
            is_staff=True,
            balance=0
        )
        self.user2.save()

    def tearDown(self):

        self.user1.delete()
        self.user2.delete()
        for coin in Coin.objects.all():
            coin.delete()
        for transaction in CoinTransaction.objects.all():
            transaction.delete()

    def test_transaction_request_works(self):

        user1_previous_balance = self.user1.balance
        user2_previous_balance = self.user2.balance
        transaction_request = TransactionRequest(
            amount=3,
            from_user=self.user1,
            to_user=self.user2,
        )

        transaction_request.execute()
        transaction_request.result.ready()

        self.user1.refresh_from_db()
        self.user2.refresh_from_db()

        self.assertEqual(self.user1.balance, user1_previous_balance - 3)
        self.assertEqual(self.user2.balance, user2_previous_balance + 3)
