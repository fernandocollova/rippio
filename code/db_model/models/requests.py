from django.db.models import F
from db_model.models.data import User
from celery_tasks.tasks import quee_mint, quee_transaction


class MintRequest():
    """A request to create a coin."""

    def __init__(self, amount, user):

        self.amount = amount
        self.user = user
        self.result = None

    def execute(self):
        """Change the balancer of the user, and quee a minting operation."""

        self.user.balance = F('balance') + self.amount
        self.user.save()
        self.result = quee_mint.delay(self.amount, self.user.pk)


class TransactionRequest():
    """A request to move coins between users."""

    def __init__(self, amount, from_user, to_user):

        self.amount = amount
        self.from_user = from_user
        self.to_user = to_user
        self.result = None

    def execute(self):
        """Change the balance of the users, and quee a transaction."""

        self.from_user.balance = F('balance') - self.amount
        self.to_user.balance = F('balance') + self.amount
        self.from_user.save()
        self.to_user.save()
        self.result = quee_transaction.delay(
            self.amount,
            self.from_user.pk,
            self.to_user.pk
        )
