from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    """Custom user implementation."""

    balance = models.IntegerField()


class Coin(models.Model):
    """The coin object you buy, sell, or transfer."""

    owner = models.ForeignKey(User, related_name="coins")


class CoinTransaction(models.Model):
    """A log of the movement of each coin"""

    coin = models.ForeignKey(Coin)
    sending = models.ForeignKey(User, related_name="seding_transactions")
    receiving = models.ForeignKey(User, related_name="receiving_transactions")
