# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2017-12-28 10:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('db_model', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='balance',
            field=models.IntegerField(),
        ),
    ]
