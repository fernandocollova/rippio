from rest_framework import generics, permissions
from db_model.models import User
from controllers.serializers import (
    MintRequestSerializer,
    TransactionRequestSerializer,
    UserSerializer,
)


class MintRequestView(generics.CreateAPIView):
    """Endpoint to create Coins."""

    serializer_class = MintRequestSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def perform_create(self, serializer):
        mint_request = serializer.save()
        mint_request.execute()


class TransactionRequestView(generics.CreateAPIView):
    """Endpoint to move coins between users."""

    serializer_class = TransactionRequestSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def perform_create(self, serializer):
        transaction_request = serializer.save()
        transaction_request.execute()

class UserView(generics.RetrieveAPIView):
    """Endpoint to get a user's information."""

    queryset = User.objects.all()

    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated,)
