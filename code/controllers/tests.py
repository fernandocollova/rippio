from db_model.models import User, Coin, CoinTransaction
from django.test import TestCase, Client
from time import sleep

from controllers.views import MintRequestView, TransactionRequestView


class TestMintEndpoint(TestCase):

    def setUp(self):

        self.password = 'admin'
        self.admin = User.objects.create_user(
            username='admin',
            password=self.password,
            email="admin@ripio.com",
            is_superuser=True,
            is_staff=True,
            balance=0
        )
        self.admin.save()

        self.client = Client()
        self.client.login(
            username=self.admin.username,
            password=self.password
        )

        self.password = 'user1'
        self.user1 = User.objects.create_user(
            username='user1',
            password=self.password,
            email="user1@ripio.com",
            is_superuser=True,
            is_staff=True,
            balance=0
        )
        self.user1.save()

    def tearDown(self):
        self.user1.delete()
        self.admin.delete()
        for coin in Coin.objects.all():
            coin.delete()

    def test_mint_works(self):
        """Minting a positive amount changes balances and responds 201"""

        mint_data = {
            "amount": 1,
            "user": self.user1.pk
        }

        response = self.client.post('/mint/', mint_data)
        user1_previous_balance = self.user1.balance
        self.user1.refresh_from_db()

        self.assertEqual(response.status_code, 201)
        self.assertEqual(self.user1.balance, user1_previous_balance + 1)

    def test_negative_mint_fails(self):
        """Minting a negative number of coins returns 400"""

        mint_data = {
            "amount": -1,
            "user": self.user1.pk
        }

        user1_previous_balance = self.user1.balance

        response = self.client.post('/mint/', mint_data)
        self.user1.refresh_from_db()

        self.assertEqual(response.status_code, 400)
        self.assertEqual(self.user1.balance, user1_previous_balance)


class TestTransactionEndpoint(TestCase):

    def setUp(self):

        self.password = 'admin'
        self.admin = User.objects.create_user(
            username='admin',
            password=self.password,
            email="admin@ripio.com",
            is_superuser=True,
            is_staff=True,
            balance=0
        )
        self.admin.save()

        self.client = Client()
        self.client.login(
            username=self.admin.username,
            password=self.password
        )

        self.password = 'user1'
        self.user1 = User.objects.create_user(
            username='user1',
            password=self.password,
            email="user1@ripio.com",
            is_superuser=True,
            is_staff=True,
            balance=10
        )
        self.user1.save()

        Coin.objects.bulk_create([Coin(owner=self.user1) for _ in range(10)])

        self.password = 'user2'
        self.user2 = User.objects.create_user(
            username='user2',
            password=self.password,
            email="user2@ripio.com",
            is_superuser=True,
            is_staff=True,
            balance=0
        )
        self.user2.save()

    def tearDown(self):
        self.user1.delete()
        self.user2.delete()
        self.admin.delete()
        for coin in Coin.objects.all():
            coin.delete()
        for transaction in CoinTransaction.objects.all():
            transaction.delete()

    def test_transaction_works(self):
        """Moving a positive amount changes balances and responds 201"""

        transaction_data = {
            "amount": 3,
            "from_user": self.user1.pk,
            "to_user": self.user2.pk
        }

        user1_previous_balance = self.user1.balance
        user2_previous_balance = self.user2.balance

        response = self.client.post('/transaction/', transaction_data)
        self.user1.refresh_from_db()
        self.user2.refresh_from_db()

        self.assertEqual(response.status_code, 201)
        self.assertEqual(self.user1.balance, user1_previous_balance - 3)
        self.assertEqual(self.user2.balance, user2_previous_balance + 3)

    def test_negative_transaction_fails(self):
        """Moving a negative amount fails with 401"""

        transaction_data = {
            "amount": -1,
            "from_user": self.user1.pk,
            "to_user": self.user2.pk
        }

        user1_previous_balance = self.user1.balance
        user2_previous_balance = self.user2.balance

        response = self.client.post('/transaction/', transaction_data)
        self.user1.refresh_from_db()
        self.user2.refresh_from_db()

        self.assertEqual(response.status_code, 400)
        self.assertEqual(self.user1.balance, user1_previous_balance)
        self.assertEqual(self.user2.balance, user2_previous_balance)

    def test_excesive_transaction_fails(self):
        """Moving more than the sender's balance fails with 401"""

        transaction_data = {
            "amount": 5000,
            "from_user": self.user1.pk,
            "to_user": self.user2.pk
        }

        user1_previous_balance = self.user1.balance
        user2_previous_balance = self.user2.balance

        response = self.client.post('/transaction/', transaction_data)
        self.user1.refresh_from_db()
        self.user2.refresh_from_db()

        self.assertEqual(response.status_code, 400)
        self.assertEqual(self.user1.balance, user1_previous_balance)
        self.assertEqual(self.user2.balance, user2_previous_balance)


class TestLogin(TestCase):

    def setUp(self):
        self.client = Client()

        self.password = 'user1'
        self.user1 = User.objects.create_user(
            username='user1',
            password=self.password,
            email="user1@ripio.com",
            is_superuser=True,
            is_staff=True,
            balance=0
        )
        self.user1.save()

        self.password = 'user2'
        self.user2 = User.objects.create_user(
            username='user2',
            password=self.password,
            email="user2@ripio.com",
            is_superuser=True,
            is_staff=True,
            balance=0
        )
        self.user2.save()

    def tearDown(self):
        self.user1.delete()
        self.user2.delete()

    def test_not_logged_post_fails(self):
        """Any action by a not logged user fails with 403"""

        mint_data = {
            "amount": 1,
            "user": self.user1.pk
        }

        transaction_data = {
            "amount": 1,
            "from_user": self.user1.pk,
            "to_user": self.user2.pk
        }

        user1_previous_balance = self.user1.balance
        user2_previous_balance = self.user2.balance

        response1 = self.client.post('/mint/', mint_data)
        response2 = self.client.post('/transaction/', transaction_data)
        response3 = self.client.post('/user/{}/'.format(self.user1.pk))
        self.user1.refresh_from_db()
        self.user2.refresh_from_db()

        self.assertEqual(response1.status_code, 403)
        self.assertEqual(response2.status_code, 403)
        self.assertEqual(response3.status_code, 403)
        self.assertEqual(self.user1.balance, user1_previous_balance)
        self.assertEqual(self.user2.balance, user2_previous_balance)
