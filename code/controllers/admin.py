from django.contrib import admin
from db_model.models import User, Coin, CoinTransaction


class UserAdmin(admin.ModelAdmin):
    pass


class CoinAdmin(admin.ModelAdmin):
    pass


class CoinTransactionAdmin(admin.ModelAdmin):
    pass


admin.site.register(User, UserAdmin)
admin.site.register(Coin, CoinAdmin)
admin.site.register(CoinTransaction, CoinTransactionAdmin)
