from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator
from db_model.models import (
    MintRequest,
    User,
    TransactionRequest,
    MintRequest,
)

class UserSerializer(serializers.ModelSerializer):
    """Serialize the User model."""

    class Meta:
        model = User
        fields = ('id', 'balance', "username", "email")

class MintRequestSerializer(serializers.Serializer):
    """Serialize the request to create a coin."""

    amount = serializers.IntegerField(min_value=1)
    user = serializers.IntegerField(min_value=1)

    def to_internal_value(self, data):
        """Add the user object to the default object representation."""

        rep = super().to_internal_value(data)
        rep["user"] = User.objects.get(pk=rep["user"])
        return rep

    def to_representation(self, instance):
        """Add the user object to the default object representation."""

        instance.user = instance.user.pk
        return super().to_representation(instance)

    def create(self, validated_data):
        return MintRequest(**validated_data)


def amount_validator(value):
    """Check the sending user has enough money for the transaction."""

    if value["from_user"].balance < value["amount"]:
        raise serializers.ValidationError(
            "You don't have enough money for this transaction."
        )


class TransactionRequestSerializer(serializers.Serializer):
    """Serialize the request to create a coin."""

    amount = serializers.IntegerField(min_value=1)
    from_user = serializers.IntegerField(min_value=1)
    to_user = serializers.IntegerField(min_value=1)

    def to_internal_value(self, data):
        """Add the user object to the default object representation."""

        rep = super().to_internal_value(data)
        rep["from_user"] = User.objects.get(pk=rep["from_user"])
        rep["to_user"] = User.objects.get(pk=rep["to_user"])
        return rep

    def to_representation(self, instance):
        """Add the user object to the default object representation."""

        instance.from_user = instance.from_user.pk
        instance.to_user = instance.to_user.pk
        return super().to_representation(instance)

    def create(self, validated_data):
        return TransactionRequest(**validated_data)

    class Meta:
        validators = [amount_validator]
