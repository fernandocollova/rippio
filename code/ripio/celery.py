import os
from celery import Celery

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ripio.settings')

app = Celery(
    'ripio',
    include=['celery_tasks.tasks'],
    broker='pyamqp://guest@rabbitmq//',
    backend='django-db',
)

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
# app.autodiscover_tasks(["celery_tasks"])
app.autodiscover_tasks()

if __name__ == '__main__':
    app.start()
