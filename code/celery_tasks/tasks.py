from celery import shared_task
from django.db import transaction
from db_model.models.data import Coin, User, CoinTransaction
from celery_tasks.exceptions import NotEnoughCoins
from ripio.celery import app


@shared_task
@transaction.atomic
def quee_mint(amount, user_pk):
    """Mint a number of conis."""

    user = User.objects.get(pk=user_pk)
    created_ids = []
    for _ in range(amount):
        coin = Coin.objects.create(owner=user)
        created_ids.append(coin.pk)
    return created_ids


@shared_task(autoretry_for=(NotEnoughCoins,), countdown=5, retry_backoff=True)
@transaction.atomic
def quee_transaction(amount, from_user_pk, to_user_pk):
    """Change the owner of a number of coins."""

    if Coin.objects.filter(owner__pk=from_user_pk).count() < amount:
        raise NotEnoughCoins()

    from_user = User.objects.get(pk=from_user_pk)
    to_user = User.objects.get(pk=to_user_pk)
    coins_to_move = Coin.objects.filter(owner=from_user)[:amount]

    created_ids = []
    for coin in coins_to_move:
        coin.owner = to_user
        coin.save()
        new_transaction = CoinTransaction.objects.create(
            coin=coin,
            sending=from_user,
            receiving=to_user
        )
        created_ids.append(new_transaction.pk)
    return created_ids
