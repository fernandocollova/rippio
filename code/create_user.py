#!python

import os
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ripio.settings")
django.setup()

from db_model.models import User

user = User.objects.create_user(
    username='ripio',
    password='ripio',
    email="ripio@ripio.com",
    is_superuser=True,
    is_staff=True,
    balance=0,
)
user.save()
print("\nNew user 'ripio' with password 'ripio' was created\n")
